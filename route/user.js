const express = require('express')
const route = express.Router()
const {

    registration,
    login,
    jwtToken,
    verifyToken
} = require('../controller/user_control')
const {
    userData

} = require('../controller/dataHandling')
// registration route
route.post('/registration', registration, (req, res) => {

    res.status(200).send('your data posted successfully')
})
// log in route
route.post('/login', login, jwtToken, (req, res) => {
    delete req.body.mail
    res.status(200).send(req.body)
})
// get user data using jwt token
route.post('/jwt', verifyToken, userData, (req, res) => {
    res.status(200).send(req.body.fetchData)
})


module.exports = route