const express = require('express')
const route = express.Router()
const {
    verifyToken
} = require('../controller/user_control')
const {
    blogPost,
    blogGet,
    blogUpdate
} = require('../controller/post_control')
// blog post route
route.post('/', verifyToken, blogPost, async (req, res) => {
    res.status(200).send('post successfully')
})
// blog get route
route.get('/:id', verifyToken, blogGet, async (req, res) => {
    res.status(200).send(res.body)
})
// blog update route
route.put('/:id', verifyToken, blogUpdate, async (req, res) => {
    res.status(200).send("updated successfully")
})





module.exports = route