const {
    blogPostModel
} = require('../data/data')


const blogPost = async (req, res, next) => {

    // try catch not work, may be err happend in others block
    try {
        const blogPostData = blogPostModel({
            postId: req.body.postId,
            postBody: req.body.postBody,
            author: req.body.data.mail
        })
        blogPostData.save()
        next()

    } catch (error) {
        res.send(error)
    }
}

const blogGet = async (req, res, next) => {
    try {
        const blogPostData = await blogPostModel.findOne({
            postId: req.params.id
        })
        res.body = blogPostData
        next()
    } catch (error) {
        res.status(401).send(error)
    }
}

const blogUpdate = async (req, res, next) => {
    try {
        const author = await blogPostModel.findOne({
            postId: req.params.id
        })
        const bool = req.body.data.mail === author?.author
        if (bool) {
            const blogPostData = await blogPostModel.updateOne({
                postId: req.params.id
            }, {
                $set: {
                    postBody: req.body.postBody
                }
            })
            next()
        } else {
            res.status(401).send("you are not authorized to update this post")
        }
    } catch (error) {
        res.status(401).send(error)
    }
}



module.exports = {
    blogPost,
    blogGet,
    blogUpdate
}