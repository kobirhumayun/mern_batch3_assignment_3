const {
    registrationModel,
} = require('../data/data')
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const req = require('express/lib/request');
// registration controller
const registration = async (req, res, next) => {

    // try catch not work, may be err happend in others block
    try {
        if (req.body.password === req.body.confirmPassword) {
            delete req.confirmPassword

            const hash = await bcrypt.hash(req.body.password, 10)
            const user = registrationModel({
                fName: req.body.fName,
                lName: req.body.lName,
                mail: req.body.mail,
                password: hash
            })
            user.save()
            next()


        } else {
            res.status(400).send("password does not match")
        }
    } catch (error) {
        res.send(error)
    }
}
// login middleware
const login = async (req, res, next) => {

    try {
        const result = await registrationModel.findOne({
            mail: req.body.mail
        }, {
            _id: 0
        })
        const match = await bcrypt.compare(req.body.password, result.password);
        if (match) {
            const temp = JSON.parse(JSON.stringify(result))
            delete temp.password
            delete temp.__v
            req.body.data = temp
            next()
        } else {
            res.status(400).send("your passward does not match")
        }
    } catch (error) {
        res.send(error)
    }
}
// token generator 
const jwtToken = (req, res, next) => {
    (async function () {
        try {
            const privateKey = "dfsalkjgfdsadfsalkj"
            const token = await jwt.sign(req.body.data, privateKey, {
                expiresIn: '1h'
            });
            req.body.token = token
            delete req.body.password
            next()
        } catch (error) {
            res.status(401).send(error)
        }
    })()
}
// verify token
const verifyToken = async (req, res, next) => {
    try {
        const token = req.body.token
        const privateKey = "dfsalkjgfdsadfsalkj"
        const decoded = jwt.verify(token, privateKey);
        req.body.data = decoded
        next()
    } catch (error) {
        res.status(401).send(error)
    }

}


module.exports = {

    registration,
    login,
    jwtToken,
    verifyToken
}