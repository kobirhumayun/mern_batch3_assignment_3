const {
    registrationModel,
} = require('../data/data')

// fetch data from database 
const userData = (req, res, next) => {
    (async function () {
        try {
            const data = await registrationModel.findOne({
                mail: req.body.data.mail
            }, {
                _id: 0
            })
            const temp = JSON.parse(JSON.stringify(data))
            delete temp.password
            delete temp.__v
            req.body.fetchData = temp
            next()
        } catch (error) {

        }
    })()
}

module.exports = {

    userData
}