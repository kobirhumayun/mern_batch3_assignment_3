const express = require("express")
const app = express()
const port = 3000
const mongoose = require('mongoose')
const user = require('./route/user')
const blogPost = require('./route/blogPost')
app.use(express.json())
app.use('/users', user)
app.use('/blogpost', blogPost)






app.all('*', (req, res) => {
    res.status(404).send("API not found")
})
app.listen(port, () => {
    console.log(`server is running on port ${port}`);
})
mongoose.connect('mongodb://localhost/databaseForMongoose')