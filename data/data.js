const mongoose = require('mongoose')
const {
    Schema
} = mongoose
// registration Schema
const registrationSchema = new Schema({
    fName: String,
    lName: String,
    mail: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    }
})

const blogPostSchema = {
    postId: {
        type: Number,
        required: true
    },
    postBody: String,
    author: String
}

const registrationModel = new mongoose.model('userData', registrationSchema)
const blogPostModel = new mongoose.model('blogPostData', blogPostSchema)
module.exports = {
    registrationModel,
    blogPostModel
}